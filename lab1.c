#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int timer = 1;
int flag = 1;
char buf [2];
char b[1];
int pipefd[2];

void handler_exit(int sig) {
    flag = 0;
}

void handler_change_alarm(int sig) {
    read(pipefd[0], b, 1);
    timer = atoi(b);

}

void handler_alarm(int sig) {
    printf("Alarm\n");

}

int main() {
    
    int pid;
    int bytes = 0;
    int fds[2];
    pipe(fds);

    
    signal(SIGINT, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    
    if ( ( pid = fork() ) == 0 ) { // Child
        
        dup2(fds[0], STDIN_FILENO);
        close(fds[0]);
        close(fds[1]);
        
        signal(SIGALRM, handler_alarm);
        signal(SIGUSR1, handler_exit);
        signal(SIGUSR2, handler_change_alarm);
        
        while(flag) {
            alarm(timer);
            pause();
        }
    } else if (pid != -1) { // Parent
        dup2(fds[1], STDOUT_FILENO);
        close(fds[0]);
        close(fds[1]);
        
        while(1) {
            bytes = read(0, &buf[0], 2);
            
            if (bytes == 0) { // Ctrl + "D"
                kill(pid, SIGUSR1);
                wait(NULL);
                exit(1);
            }
            

            if ((buf[0] == 49 || buf[0] == 50 || buf[0] == 51 || buf[0] == 52 || buf[0] == 53 || buf[0] == 54 || buf[0] == 55
                 || buf[0] == 56 || buf[0] == 57) && buf[1] == 10){
        
           
            
            write(STDOUT_FILENO, buf, 1);
            
                           kill(pid, SIGUSR2);
            
            }
    
       
        }
    } else {
        printf("Error in fork");
    }
}
